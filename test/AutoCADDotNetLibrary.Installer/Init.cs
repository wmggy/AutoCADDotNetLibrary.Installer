﻿using System.Threading;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;

namespace AutoCADDotNetLibrary.Installer
{
    public class Init : IExtensionApplication
    {
        public void Initialize()
        {
            SetMutex();
            Application.ShowAlertDialog("AutoCADDotNetLibrary.Installer");
        }

        public void Terminate()
        {
        }

        private static Mutex _m;
        //防止安装程序运行。
        public static void SetMutex()
        {
            string guid = "{CE6951D4-9479-425D-80EA-197293294FFB}";
            Mutex.TryOpenExisting(guid, out _m);
            if (_m == null)
            {
                _m = new Mutex(false, guid);
                _m.WaitOne();
            }
        }
    }
}