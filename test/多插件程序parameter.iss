//下列参数的目的是解决安装程序中有多个插件程序。
//AutoCAD.iss的多插件程序参数，继承自单插件程序参数。可以向parameter.iss文件添加下列参数。
//参数只影响注册表。测试时，只查看注册表即可。



#define AutoCADProgramLoader1 "1.arx"                         ;插件程序位置，可以为dll或者arx，使用'{app}\'拼接              
#define AutoCADProgramMinVersion1 "2013"                      ;支持AutoCAD最小版本                 
#define AutoCADProgramMaxVersion1 "2024"                      ;支持AutoCAD最大版本                 
#define AutoCADProgramDescription1 "11"                       ;.NET 程序集的说明，可省略，默认为程序名称              
#define AutoCADProgramLoadctrls1 "11"                         ;控制加载 .NET 程序集的方式和时间，可省略，默认为2                  

#define AutoCADProgramLoader2 "2.dll"              
#define AutoCADProgramMinVersion2 "2013"                                       
#define AutoCADProgramMaxVersion2 "2015"                                       
#define AutoCADProgramDescription2 "22"                                     
#define AutoCADProgramLoadctrls2 "22"

#define AutoCADProgramLoader3 "3\3.dll"             
#define AutoCADProgramMinVersion3 "2013"                                       
#define AutoCADProgramMaxVersion3 "2024"                                       
#define AutoCADProgramDescription3 "33"                                     
#define AutoCADProgramLoadctrls3 "33"

#define AutoCADProgramLoader4 "3\4.arx"              
#define AutoCADProgramMinVersion4 "2013"                                       
#define AutoCADProgramMaxVersion4 "2015"                                       
#define AutoCADProgramDescription4 "44"                                     
#define AutoCADProgramLoadctrls4 "44"

//依次类推，直至参数9