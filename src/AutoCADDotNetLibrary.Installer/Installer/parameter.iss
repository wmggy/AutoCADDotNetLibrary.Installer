//插件程序参数


//如果msbuild的属性需要与下文中的变量关联，则需要修改msbuild的属性`ISSParameter`，并使用`;`进行注释下文中的变量。可以参考`AutoCADProgramPath`和`AutoCADProjectOutputDir`。


;#define AutoCADProgramPath "..\test\插件程序\AutoCADDotNetLibrary.Installer.dll"     ;插件程序的路径，可测试使用
#if !FileExists(AutoCADProgramPath)
  #error 不存在插件程序
#endif


#define AppId "{7f1026ce-764b-499c-8202-562d3b336b2b}"                                ;id，软件唯一标识。在复制此文件时，此值会被msbuild更改。
#define AppName RemoveFileExt(ExtractFileName(AutoCADProgramPath))                    ;名称
#define AppVersion GetVersionNumbersString(AutoCADProgramPath)                        ;版本
#define AppPublisher="My Company, Inc."                                               ;发布者
;#define AppURL="https://www.example.com/"                                            ;网址，可省略

#define AutoCADProjectOutputName AppName                                              ;安装程序名称
;#define AutoCADProjectOutputDir ".\Output"                                           ;安装程序目录
#define AutoCADProgramDir ExtractFileDir(AutoCADProgramPath)                          ;插件程序目录															            	 



//AutoCAD.iss的插件程序参数。此功能只影响注册表。												  	       	            
#define SelectAutoCADVersion "Last"                                                   ;默认选择AutoCAD的版本，可以为All，First，Last，可省略，默认为Last
#define AutoCADProgramLoader ExtractFileName(AutoCADProgramPath)                      ;插件程序位置，可以为dll或者arx，使用'{app}\'拼接，不会检查存在性
#define AutoCADProgramMinVersion "2013"                                               ;支持AutoCAD最小版本
#define AutoCADProgramMaxVersion "2024"                                               ;支持AutoCAD最大版本
#define AutoCADProgramDescription AppName                                             ;.NET 程序集的说明，可省略，默认为程序名称
#define AutoCADProgramLoadctrls "2"                                                   ;控制加载 .NET 程序集的方式和时间，可省略，默认为2
