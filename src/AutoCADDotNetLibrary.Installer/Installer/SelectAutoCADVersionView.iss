; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

; AutoCAD版本选择框
; 根据AutoCADProgramMinVersion，AutoCADProgramMaxVersion范围和用户已安装的AutoCAD版本显示UI界面。安装时，先删除之前安装的旧注册表信息，然后根据用户选择的安装版本写入注册表信息。此功能只影响注册表。
; AutoCAD注册表：https://help.autodesk.com/view/OARX/2019/CHS/?guid=GUID-70D60274-57E0-4B22-8D0C-3C7F212A7CAF


#define AutoCADLatestVersion 2030                                 ;AutoCAD的最新的版本

#dim AutoCADVersion[AutoCADLatestVersion + 1]                     ;AutoCAD的版本号值
#define AutoCADVersion[2006] "R16.2"
#define AutoCADVersion[2007] "R17.0"
#define AutoCADVersion[2008] "R17.1"
#define AutoCADVersion[2009] "R17.2"
#define AutoCADVersion[2010] "R18.0"
#define AutoCADVersion[2011] "R18.1"
#define AutoCADVersion[2012] "R18.2"
#define AutoCADVersion[2013] "R19.0"
#define AutoCADVersion[2014] "R19.1"
#define AutoCADVersion[2015] "R20.0"
#define AutoCADVersion[2016] "R20.1"
#define AutoCADVersion[2017] "R21.0"
#define AutoCADVersion[2018] "R22.0"
#define AutoCADVersion[2019] "R23.0"
#define AutoCADVersion[2020] "R23.1"
#define AutoCADVersion[2021] "R24.0"
#define AutoCADVersion[2022] "R24.1"
#define AutoCADVersion[2023] "R24.2"
#define AutoCADVersion[2024] "R24.3"  


[Registry]
;根据选择的AutoCAD版本和定义参数，创建注册表
#define AutoCADVersionLoop 2006       ;AutoCAD版本数量循环参数，2006-AutoCADLatestVersion
#define ProgramLoop 0                 ;插件程序数量循环参数，0-9

#sub CreateRegistry
Root: HKCU; Subkey: "Software\Autodesk\AutoCAD\{code:GetAutoCADVersion|{#AutoCADVersionLoop}}\{code:GetAutoCurVer|{#AutoCADVersionLoop}}\Applications\{code:GetAutoCADProgramName|{#ProgramLoop}}"; ValueType: string; ValueName: "DESCRIPTION"; ValueData: "{code:GetAutoCADProgramDescription|{#ProgramLoop}}"; Check: IsInstallAutoCADFunc({#AutoCADVersionLoop}, {#ProgramLoop}); Flags: uninsdeletekey                                                                                                                                            
Root: HKCU; Subkey: "Software\Autodesk\AutoCAD\{code:GetAutoCADVersion|{#AutoCADVersionLoop}}\{code:GetAutoCurVer|{#AutoCADVersionLoop}}\Applications\{code:GetAutoCADProgramName|{#ProgramLoop}}"; ValueType: dword; ValueName: "LOADCTRLS"; ValueData: "{code:GetAutoCADProgramLoadctrls|{#ProgramLoop}}"; Check: IsInstallAutoCADFunc({#AutoCADVersionLoop}, {#ProgramLoop});                                                                                                                                    
Root: HKCU; Subkey: "Software\Autodesk\AutoCAD\{code:GetAutoCADVersion|{#AutoCADVersionLoop}}\{code:GetAutoCurVer|{#AutoCADVersionLoop}}\Applications\{code:GetAutoCADProgramName|{#ProgramLoop}}"; ValueType: string; ValueName: "LOADER"; ValueData: "{app}\{code:GetAutoCADProgramLoader|{#ProgramLoop}}"; Check: IsInstallAutoCADFunc({#AutoCADVersionLoop}, {#ProgramLoop});                                                                                                                                              
Root: HKCU; Subkey: "Software\Autodesk\AutoCAD\{code:GetAutoCADVersion|{#AutoCADVersionLoop}}\{code:GetAutoCurVer|{#AutoCADVersionLoop}}\Applications\{code:GetAutoCADProgramName|{#ProgramLoop}}"; ValueType: dword; ValueName: "MANAGED"; ValueData: "1"; Check: IsNetAssemblyFunc({#AutoCADVersionLoop}, {#ProgramLoop});
#endsub

;根据AutoCAD版本循环
#sub CreateAllVersionRegistry
  #for {AutoCADVersionLoop = 2006; AutoCADVersionLoop < AutoCADLatestVersion + 1; AutoCADVersionLoop++} CreateRegistry
#endsub

;根据插件程序数量循环
#for {ProgramLoop = 0; ProgramLoop < 10; ProgramLoop++} CreateAllVersionRegistry


[Code]
var                                                                             //Arr指插件程序个数。
  SelectAutoCADVersion: String;                                                 //选择AutoCAD版本界面的方式All，First，Last，默认Last。
  AutoCADProgramDescriptionArr: array [0..9] of String;                         //.NET 程序集的说明，并且是可选的。
  AutoCADProgramLoadctrlsArr: array [0..9] of String;                           //控制加载 .NET 程序集的方式和时间。
  AutoCADProgramLoaderArr: array [0..9] of String;                              //指定要加载的 .NET 程序集文件，也可以为 ObjectARX 文件。
  AutoCADProgramMinVersionArr: array [0..9] of Integer;                         //插件程序的最小版本。
  AutoCADProgramMaxVersionArr: array [0..9] of Integer;                         //插件程序的最大版本。
                                                                                
  Page: TInputOptionWizardPage;                                                 //选择AutoCAD版本界面
  AutoCADMinVersion: Integer;                                                   //AutoCADProgramMinVersionArr的最小值
  AutoCADMaxVersion: Integer;                                                   //AutoCADProgramMaxVersionArr的最大值
  AutoCADVersion: array [2006..{#AutoCADLatestVersion}] of String;              //注册表：R24.1
  AutoCurVer: array [2006..{#AutoCADLatestVersion}] of String;                  //注册表：ACAD-5101:804
  IsInstallAutoCAD: array [2006..{#AutoCADLatestVersion}] of Integer;           //用户是否安装了AutoCAD对应的版本

function GetAutoCADVersion(Param: String): String;
begin
  Result := AutoCADVersion[StrToInt(Param)];
end;

function GetAutoCurVer(Param: String): String;
begin
  Result := AutoCurVer[StrToInt(Param)];
end;

//程序名
function GetAutoCADProgramName(Param: String): String;
begin
  Result := ChangeFileExt(ExtractFileName(AutoCADProgramLoaderArr[StrToInt(Param)]), '');
end;

//用户选择安装的AutoCAD版本，Param1：选择的AutoCAD版本，Param2：选择的插件程序
function IsInstallAutoCADFunc(Param1: Integer; Param2: Integer): Boolean;
begin
  if(IsInstallAutoCAD[Param1] <> -1) then 
  if(AutoCADProgramMinVersionArr[Param2] <= Param1) then 
  if(AutoCADProgramMaxVersionArr[Param2] >= Param1) then 
    Result := Page.Values[IsInstallAutoCAD[Param1]]; 
end;

//DESCRIPTION
function GetAutoCADProgramDescription(Param: String): String;
begin
  Result := ChangeFileExt(ExtractFileName(AutoCADProgramLoaderArr[StrToInt(Param)]), '');

  if AutoCADProgramDescriptionArr[StrToInt(Param)] <> '' then
    Result := AutoCADProgramDescriptionArr[StrToInt(Param)];
end;

//LOADCTRLS
function GetAutoCADProgramLoadctrls(Param: String): String;
begin
  Result := '2';

  if AutoCADProgramLoadctrlsArr[StrToInt(Param)] <> '' then
    Result := AutoCADProgramLoadctrlsArr[StrToInt(Param)];
end;

//LOADER
function GetAutoCADProgramLoader(Param: String): String;
begin
  Result := AutoCADProgramLoaderArr[StrToInt(Param)];
end;

//MANAGED
function IsNetAssemblyFunc(Param1: Integer; Param2: Integer): Boolean;
begin
  if IsInstallAutoCADFunc(Param1, Param2) then
  if ExtractFileExt(AutoCADProgramLoaderArr[Param2]) = '.dll' then
    Result := True;
end;


//初始化，赋值
<event('InitializeSetup')>
function AutoCADInitializeSetup(): Boolean;
var
  i: integer;
begin

  //赋值
  SelectAutoCADVersion := 'Last';
#ifdef SelectAutoCADVersion
  SelectAutoCADVersion := '{#SelectAutoCADVersion}';
#endif

#ifdef AutoCADProgramLoader
  AutoCADProgramLoaderArr[0] := '{#AutoCADProgramLoader}';
  AutoCADProgramMinVersionArr[0] := StrToInt('{#AutoCADProgramMinVersion}');
  AutoCADProgramMaxVersionArr[0] := StrToInt('{#AutoCADProgramMaxVersion}');
#endif
#ifdef AutoCADProgramLoader1
  AutoCADProgramLoaderArr[1] := '{#AutoCADProgramLoader1}';
  AutoCADProgramMinVersionArr[1] := StrToInt('{#AutoCADProgramMinVersion1}');
  AutoCADProgramMaxVersionArr[1] := StrToInt('{#AutoCADProgramMaxVersion1}');
#endif
#ifdef AutoCADProgramLoader2
  AutoCADProgramLoaderArr[2] := '{#AutoCADProgramLoader2}';
  AutoCADProgramMinVersionArr[2] := StrToInt('{#AutoCADProgramMinVersion2}');
  AutoCADProgramMaxVersionArr[2] := StrToInt('{#AutoCADProgramMaxVersion2}');
#endif
#ifdef AutoCADProgramLoader3
  AutoCADProgramLoaderArr[3] := '{#AutoCADProgramLoader3}';
  AutoCADProgramMinVersionArr[3] := StrToInt('{#AutoCADProgramMinVersion3}');
  AutoCADProgramMaxVersionArr[3] := StrToInt('{#AutoCADProgramMaxVersion3}');
#endif
#ifdef AutoCADProgramLoader4
  AutoCADProgramLoaderArr[4] := '{#AutoCADProgramLoader4}';
  AutoCADProgramMinVersionArr[4] := StrToInt('{#AutoCADProgramMinVersion4}');
  AutoCADProgramMaxVersionArr[4] := StrToInt('{#AutoCADProgramMaxVersion4}');
#endif
#ifdef AutoCADProgramLoader5
  AutoCADProgramLoaderArr[5] := '{#AutoCADProgramLoader5}';
  AutoCADProgramMinVersionArr[5] := StrToInt('{#AutoCADProgramMinVersion5}');
  AutoCADProgramMaxVersionArr[5] := StrToInt('{#AutoCADProgramMaxVersion5}');
#endif
#ifdef AutoCADProgramLoader6
  AutoCADProgramLoaderArr[6] := '{#AutoCADProgramLoader6}';
  AutoCADProgramMinVersionArr[6] := StrToInt('{#AutoCADProgramMinVersion6}');
  AutoCADProgramMaxVersionArr[6] := StrToInt('{#AutoCADProgramMaxVersion6}');
#endif
#ifdef AutoCADProgramLoader7
  AutoCADProgramLoaderArr[7] := '{#AutoCADProgramLoader7}';
  AutoCADProgramMinVersionArr[7] := StrToInt('{#AutoCADProgramMinVersion7}');
  AutoCADProgramMaxVersionArr[7] := StrToInt('{#AutoCADProgramMaxVersion7}');
#endif
#ifdef AutoCADProgramLoader8
  AutoCADProgramLoaderArr[8] := '{#AutoCADProgramLoader8}';
  AutoCADProgramMinVersionArr[8] := StrToInt('{#AutoCADProgramMinVersion8}');
  AutoCADProgramMaxVersionArr[8] := StrToInt('{#AutoCADProgramMaxVersion8}');
#endif
#ifdef AutoCADProgramLoader9
  AutoCADProgramLoaderArr[9] := '{#AutoCADProgramLoader9}';
  AutoCADProgramMinVersionArr[9] := StrToInt('{#AutoCADProgramMinVersion9}');
  AutoCADProgramMaxVersionArr[9] := StrToInt('{#AutoCADProgramMaxVersion9}');
#endif


#ifdef AutoCADProgramDescription
  AutoCADProgramDescriptionArr[0] := '{#AutoCADProgramDescription}';
#endif
#ifdef AutoCADProgramDescription1
  AutoCADProgramDescriptionArr[1] := '{#AutoCADProgramDescription1}';
#endif
#ifdef AutoCADProgramDescription2
  AutoCADProgramDescriptionArr[2] := '{#AutoCADProgramDescription2}';
#endif
#ifdef AutoCADProgramDescription3
  AutoCADProgramDescriptionArr[3] := '{#AutoCADProgramDescription3}';
#endif
#ifdef AutoCADProgramDescription4
  AutoCADProgramDescriptionArr[4] := '{#AutoCADProgramDescription4}';
#endif
#ifdef AutoCADProgramDescription5
  AutoCADProgramDescriptionArr[5] := '{#AutoCADProgramDescription5}';
#endif
#ifdef AutoCADProgramDescription6
  AutoCADProgramDescriptionArr[6] := '{#AutoCADProgramDescription6}';
#endif
#ifdef AutoCADProgramDescription7
  AutoCADProgramDescriptionArr[7] := '{#AutoCADProgramDescription7}';
#endif
#ifdef AutoCADProgramDescription8
  AutoCADProgramDescriptionArr[8] := '{#AutoCADProgramDescription8}';
#endif
#ifdef AutoCADProgramDescription9
  AutoCADProgramDescriptionArr[9] := '{#AutoCADProgramDescription9}';
#endif


#ifdef AutoCADProgramLoadctrls
  AutoCADProgramLoadctrlsArr[0] := '{#AutoCADProgramLoadctrls}';
#endif
#ifdef AutoCADProgramLoadctrls1
  AutoCADProgramLoadctrlsArr[1] := '{#AutoCADProgramLoadctrls1}';
#endif
#ifdef AutoCADProgramLoadctrls2
  AutoCADProgramLoadctrlsArr[2] := '{#AutoCADProgramLoadctrls2}';
#endif
#ifdef AutoCADProgramLoadctrls3
  AutoCADProgramLoadctrlsArr[3] := '{#AutoCADProgramLoadctrls3}';
#endif
#ifdef AutoCADProgramLoadctrls4
  AutoCADProgramLoadctrlsArr[4] := '{#AutoCADProgramLoadctrls4}';
#endif
#ifdef AutoCADProgramLoadctrls5
  AutoCADProgramLoadctrlsArr[5] := '{#AutoCADProgramLoadctrls5}';
#endif
#ifdef AutoCADProgramLoadctrls6
  AutoCADProgramLoadctrlsArr[6] := '{#AutoCADProgramLoadctrls6}';
#endif
#ifdef AutoCADProgramLoadctrls7
  AutoCADProgramLoadctrlsArr[7] := '{#AutoCADProgramLoadctrls7}';
#endif
#ifdef AutoCADProgramLoadctrls8
  AutoCADProgramLoadctrlsArr[8] := '{#AutoCADProgramLoadctrls8}';
#endif
#ifdef AutoCADProgramLoadctrls9
  AutoCADProgramLoadctrlsArr[9] := '{#AutoCADProgramLoadctrls9}';
#endif

  
  //求值AutoCADMinVersion和AutoCADMaxVersion
  AutoCADMinVersion := {#AutoCADLatestVersion};
  for i := 0 to 9 do
  begin
    if AutoCADProgramMinVersionArr[i] >= 2006 then
    if AutoCADProgramMinVersionArr[i] < AutoCADMinVersion then 
    begin 
      AutoCADMinVersion := AutoCADProgramMinVersionArr[i];
    end;
    if AutoCADProgramMaxVersionArr[i] > AutoCADMaxVersion then 
    begin 
      AutoCADMaxVersion := AutoCADProgramMaxVersionArr[i];
    end;
  end;
  
  //求值AutoCADVersion
#sub SetAutoCADVersion
  AutoCADVersion[{#emit AutoCADVersionLoop}] := '{#emit AutoCADVersion[AutoCADVersionLoop]}';
#endsub
#for {AutoCADVersionLoop = 2006; AutoCADVersionLoop < AutoCADLatestVersion + 1; AutoCADVersionLoop++} SetAutoCADVersion

  //求值AutoCurVer
  for i := AutoCADMinVersion to AutoCADMaxVersion do
  begin
    if RegQueryStringValue(HKEY_CURRENT_USER, 'Software\Autodesk\AutoCAD\' + AutoCADVersion[i], 'CurVer', AutoCurVer[i]) then 
    begin 
      Result := True;
    end;
  end;

  if not Result then MsgBox('没有发现平台AutoCAD' + IntToStr(AutoCADMinVersion) + ' - AutoCAD' + IntToStr(AutoCADMaxVersion), mbError, MB_OK);  
end;

//选择AutoCAD版本的界面
procedure SelectAutoCADVersionUI;
var
  index: Integer;
  i: integer;
begin
  Page := CreateInputOptionPage(wpSelectDir, '选择安装AutoCAD的平台', '适用范围为AutoCAD' + IntToStr(AutoCADMinVersion) + ' - AutoCAD' + IntToStr(AutoCADMaxVersion), '安装平台', False, True);

  for i := AutoCADMinVersion to AutoCADMaxVersion do
    IsInstallAutoCAD[i] := -1;

  for i := AutoCADMinVersion to AutoCADMaxVersion do
  begin
    if RegQueryStringValue(HKEY_CURRENT_USER, 'Software\Autodesk\AutoCAD\' + AutoCADVersion[i], 'CurVer', AutoCurVer[i]) then 
    begin 
      IsInstallAutoCAD[i] := Page.Add('AutoCAD ' + IntToStr(i)); 
      index := IsInstallAutoCAD[i]; 
    end;
  end;

  if(SelectAutoCADVersion = 'Last') then 
  if(index <> -1) then 
    Page.Values[index] := True;

  if(SelectAutoCADVersion = 'First') then 
    Page.Values[0] := True;

  if(SelectAutoCADVersion = 'All') then 
    for i := 0 to Index do
    begin
      Page.Values[i] := True;
    end;
end;

<event('InitializeWizard')>
procedure AutoCADInitializeWizard();
begin
  SelectAutoCADVersionUI;
end;

//删除之前的注册表
<event('CurPageChanged')>
procedure AutoCADCurPageChanged(CurPageID: Integer);
var
  i: Integer;
  j: integer;
begin
  if CurPageID = 11 then
  for i := AutoCADMinVersion to AutoCADMaxVersion do
  for j := 0 to 9 do
  if AutoCADVersion[i] <> '' then
  if AutoCurVer[i] <> '' then
  if AutoCADProgramLoaderArr[j] <> '' then
    RegDeleteKeyIncludingSubkeys(HKEY_CURRENT_USER, 'Software\Autodesk\AutoCAD\' + AutoCADVersion[i] + '\' + AutoCurVer[i] + '\Applications\' + ChangeFileExt(ExtractFileName(AutoCADProgramLoaderArr[j]), ''))
end;

//显示AutoCAD信息
//function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoComponentsInfo, MemoTasksInfo: String): String;
//var
//  i: integer;
//begin
//  if MemoUserInfoInfo <> '' then
//    Result := MemoUserInfoInfo + Newline + NewLine;
//  
//  if MemoDirInfo <> '' then
//    Result := Result + MemoDirInfo + Newline + NewLine;
//  
//  if MemoTypeInfo <> '' then
//    Result := Result + MemoTypeInfo + Newline + NewLine;
//  
//  if MemoComponentsInfo <> '' then
//    Result := Result + MemoComponentsInfo + Newline + NewLine;
//  
//  if MemoComponentsInfo <> '' then
//    Result := Result + MemoComponentsInfo + Newline + NewLine;
//  
//  if MemoTasksInfo <> '' then
//    Result := Result + MemoTasksInfo + Newline + NewLine;
//  
//  for i := AutoCADMinVersion to AutoCADMaxVersion do
//  begin
//    if IsInstallAutoCAD[i] <> -1 then 
//    if Page.Values[IsInstallAutoCAD[i]] then
//      Result := Result + '选择安装的平台：AutoCAD ' + IntToStr(i) + Newline + NewLine;
//  end;
//end;

