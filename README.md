# AutoCADDotNetLibrary.Installer

提供`AutoCAD`二次开发制作安装程序的功能。

## 快速使用

在项目（.csproj）中使用如下示例：

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>net472</TargetFramework>

    <IsUseInnoSetup>true</IsUseInnoSetup>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="AutoCADDotNetLibrary.Installer" Version="0.8.*" />
  </ItemGroup>

</Project>
```

## 属性

| 属性             | 默认值                                                                         | 说明                                                                  |
| ---------------- | ------------------------------------------------------------------------------ | --------------------------------------------------------------------- |
| `IsUseInnoSetup` | `false`                                                                        | 是否使用[Inno Setup](https://jrsoftware.org/isinfo.php)制作安装程序。 |
| `CopyISSFileDir` | `$(ProjectDir)Installer\`                                                      | 复制`nuget`包中的预设文件。                                           |
| `MainISS`        | `main.iss`                                                                     | 脚本的入口。                                                          |
| `ISSParameter`   | `/DAutoCADProgramPath=$(TargetPath) /DAutoCADProjectOutputDir=$(TargetDir)..\` | `ISCC.exe`的参数。                                                    |

> $(TargetPath)等的[相关资料地址](https://learn.microsoft.com/zh-cn/visualstudio/ide/reference/pre-build-event-post-build-event-command-line-dialog-box?view=vs-2022)。

## 预设文件

`AutoCADDotNetLibrary.Installer`只考虑`AutoCAD二次开发`这一个业务，提供**预设文件**。由于[Inno Setup](https://jrsoftware.org/isinfo.php)博大精深，只能根据`AutoCAD二次开发`的需求，提供一般性的功能。如果有**业务性**的特殊需求，则需要具体的**重写**。如果无特殊需求，则应只修改其中的[parameter.iss](./tools/parameter.iss)文件。

当`IsUseInnoSetup`为`true`时，如果没有**预设文件**，则**预设文件**复制到项目`CopyISSFileDir`的指定位置，然后使用[Tools.InnoSetup 6.2.2](https://www.nuget.org/packages/Tools.InnoSetup)中的`ISCC.exe`程序，根据预设参数`ISSParameter`和脚本的入口`MainISS`，制作安装程序。

```
- ChineseSimplified.isl
- license.txt
- main.iss                     ;程序入口，提供一些基础的设置。
- ndp472-kb4054531-web.exe     ;net472的web运行时，提供程序运行环境。
- parameter.iss                ;通用参数，需要根据业务进行修改。
- SelectAutoCADVersionView.iss ;关于AutoCAD二次开发业务的主要的功能，不建议修改。
- 图标.ico
```

## 功能

### SelectAutoCADVersionView.iss

[SelectAutoCADVersionView.iss](/tools/SelectAutoCADVersionView.iss) ：AutoCAD 版本选择框，此脚本安装时写入[注册表](https://help.autodesk.com/view/OARX/2019/CHS/?guid=GUID-70D60274-57E0-4B22-8D0C-3C7F212A7CAF)信息。

![SelectAutoCADVersionView](/docs/img/SelectAutoCADVersionView.png)

根据`AutoCADProgramMinVersion`，`AutoCADProgramMaxVersion`范围和用户已安装的 AutoCAD 版本显示 UI 界面。安装时，先删除之前安装的旧注册表信息，然后根据用户选择的安装版本写入注册表信息。此功能只影响注册表。

#### 单插件程序参数

| 参数                        | 说明                                                                    |
| --------------------------- | ----------------------------------------------------------------------- |
| `SelectAutoCADVersion`      | 默认选择 AutoCAD 的版本，可以为 All，First，Last，可省略，默认为 Last。 |
| `AutoCADProgramLoader`      | 插件程序位置，可以为`dll`或者`arx`，使用'{app}\'拼接，不会检查存在性。  |
| `AutoCADProgramMinVersion`  | 支持 AutoCAD 最小版本。                                                 |
| `AutoCADProgramMaxVersion`  | 支持 AutoCAD 最大版本。                                                 |
| `AutoCADProgramDescription` | .NET 程序集的说明，可省略，默认为程序名称。                             |
| `AutoCADProgramLoadctrls`   | 控制加载 .NET 程序集的方式和时间，可省略，默认为 2。                    |

#### 多插件程序参数

`SelectAutoCADVersionView.iss`允许安装程序中有多个插件程序，请查看[示例](/test/多插件程序parameter.iss)，可以双击[多插件测试.bat](/test/多插件测试.bat)去运行。

## QQ 群

![QQ群](/docs/img/AutoCADDotNetLibrary%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
